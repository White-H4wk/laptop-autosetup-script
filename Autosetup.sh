#!/bin/bash

BLUE='\033[1;34m'
PURPLE='\033[1;35m'
CYAN='\033[1;36m'	
DEF='\033[0m'


if [[ $(/usr/bin/id -u) -ne 0 ]]; then
    echo "Not running as root"
    exit
fi

apt-get install git -y
apt-get install gdb -y
apt-get install build-essential -y

printf "${BLUE}Installing Vega"
apt-get install libwebkitgtk-1.0 -y
cd /opt/
wget https://dist.subgraph.com/downloads/VegaBuild-linux.gtk.x86_64.zip
unzip VegaBuild-linux.gtk.x86_64.zip
cd vega/
chmod +x Vega
ln -s /opt/vega/Vega /usr/local/bin/vega
cd ..
rm VegaBuild-linux.gtk.x86_64.zip

printf "${PURPLE}Installing Zap Proxy"
sh -c "echo 'deb http://download.opensuse.org/repositories/home:/cabelo/xUbuntu_16.04/ /' > /etc/apt/sources.list.d/owasp-zap.list"
apt-get update
apt-get install owasp-zap -y --allow-unauthenticated
ln -s /usr/share/owasp-zap/zap.sh /usr/local/owasp-zap


printf "${CYAN}Installing Nmap"
wget https://nmap.org/dist/nmap-7.60.tar.bz2
bzip2 -cd nmap-7.60.tar.bz2 | tar xvf -
cd nmap-7.60
./configure
make
make install
cd ..
rm nmap-7.60.tar.bz2


printf "${BLUE}Installing Etcher"
wget https://github.com/resin-io/etcher/releases/download/v1.1.2/etcher-1.1.2-linux-x86_64.zip
unzip etcher-1.1.2-linux-x86_64.zip
chmod +x etcher-1.1.2-linux-x86_64.AppImage
/opt/etcher-1.1.2-linux-x86_64.AppImage
rm etcher-1.1.2-linux-x86_64.zip


printf "${PURPLE}Installing Sasm"
wget download.opensuse.org/repositories/home:/Dman95/xUbuntu_16.04/amd64/sasm_3.9.0_amd64.deb
apt-get -f install nasm
dpkg -i sasm_3.9.0_amd64.deb
rm sasm_3.9.0_amd64.deb

printf "${CYAN}Installing Vim"
apt-get install libncurses5-dev libgnome2-dev libgnomeui-dev \
    libgtk2.0-dev libatk1.0-dev libbonoboui2-dev \
    libcairo2-dev libx11-dev libxpm-dev libxt-dev python-dev \
    python3-dev ruby-dev lua5.1 lua5.1-dev libperl-dev -y
git clone https://github.com/vim/vim.git
cd vim/
./configure --with-features=huge \
            --enable-multibyte \
            --enable-rubyinterp=yes \
            --enable-python3interp=yes \
            --with-python3-config-dir=/usr/lib/python3.5/ \    # Change path accordingto your system
            --enable-perlinterp=yes \
            --enable-luainterp=yes \
            --enable-gui=gtk2 \
            --enable-cscope \
            --prefix=/usr/local
make VIMRUNTIMEDIR=/usr/local/share/vim/vim80
make install
cd ..
rm -R vim
git clone https://github.com/sebdah/vim-ide.git  # Vim config that will be automatically installed
cd vim-ide/
chmod +x install.sh
./install.sh
cd ..

printf "${BLUE}Install Netbeans C/C++"
wget http://download.netbeans.org/netbeans/8.2/final/bundles/netbeans-8.2-cpp-linux-x64.sh
chmod +x netbeans-8.2-cpp-linux-x64.sh
./netbeans-8.2-cpp-linux-x64.sh
rm netbeans-8.2-cpp-linux-x64.sh

#echo "Downloading Python Plugin for Netbeans"
#wget plugins.netbeans.org/download/plugin/4221

printf "${PURPLE}Installing Valgrind"
apt-get install valgrind -y



printf "${CYAN}Install Sublime"
add-apt-repository ppa:webupd8team/sublime-text-3 -y
apt-get update
apt-get install sublime-text-installer -y

printf "${BLUE}Installing GEF"
apt-get install python3-pip
pip3 install --upgrade pip
wget -q -O- https://github.com/hugsy/gef/raw/master/gef.sh | sh
pip3 install capstone unicorn keystone-engine ropper retdec-python


printf "${PURPLE}Install Radare2/Pyew/Cutter"
apt-get install swig -y

git clone https://github.com/radare/radare2.git
chmod +rwx -R radare2
cd radare2/sys
./install.sh
cd ../..
wget https://github.com/radareorg/cutter/releases/download/untagged-da49f8730bcedb6dd1d2/Cutter-x86_64.AppImage
chmod +x Cutter-x86_64.AppImage

# -------------------------------------------------------------------Setup Bokken GUI for Pyew and Radare2 -------------------
# r2pm init
# r2pm update
# r2pm install lang-python2 #lang-python3 for python3 bindings
# r2pm install r2api-python
# r2pm install r2pipe-py

#apt-get install python2.7-dev # Bokken doesnt support python3+ yet
#apt-get install graphviz -y
#apt-get install python-gtk2 -y
#apt-get install python-gtksourceview2 -y
#apt-get install graphviz-dev
#apt-get install flex
#apt-get install bison
#apt-get install autoconf

#wget https://inguma.eu/attachments/download/212/bokken-1.8.tar.gz --no-check-certificate
#tar -zxvf bokken-1.8.tar.gz
#rm bokken-1.8.tar.gz
#git clone https://github.com/joxeankoret/pyew.git
#cd pyew/
#cd ..
#--------------------------------------------------------------------------------------------------------------------------------
apt-get install pyew -y  #Install Pyew

printf "${BLUE}Install KiCad"
apt-get install kicad -y

printf "${CYAN}Installing MAT"
apt-get install mat -y

printf "${PURPLE}Installing yEd"
wget https://www.yworks.com/resources/yed/demo/yEd-3.17.1_64-bit_setup.sh
chmod +x yEd-3.17.1_64-bit_setup.sh
./yEd-3.17.1_64-bit_setup.sh
rm yEd-3.17.1_64-bit_setup.sh


printf "${DEF}"


